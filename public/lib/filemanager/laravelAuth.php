<?php

/*
|--------------------------------------------------------------------------
| Sharing Laravel's session and checking authentication
|--------------------------------------------------------------------------
|
*/

require '../../../bootstrap/autoload.php';
$app = require_once '../../../bootstrap/app.php';
$app->make('Illuminate\Contracts\Http\Kernel')
    ->handle(Illuminate\Http\Request::capture());

class laravelAuth {

    public function authenticate() {

        if (Auth::check()) {
            if (Auth::user()->hasRole('admin')) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

        // Catch all, default to false.
        return false;

    }
}
