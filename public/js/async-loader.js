!function(window)
{
  var hashCode = function(str){
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
      var char = str.charCodeAt(i);
      hash = ((hash<<5)-hash)+char;
      hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
  }

  var AsyncLoader = {
    load: function(scriptFullUrl, callback)
    {
      var js, fjs = document.getElementsByTagName('script')[0], id= hashCode(scriptFullUrl);

      if (document.getElementById(id)) {
        return;
      }

      js=document.createElement('script'); js.id=id; js.async=1;

      js.src=scriptFullUrl;
      js.addEventListener('load', function(e) {
        if (typeof  callback === 'function') {
          callback.call()
        }
      }, false)
      fjs.parentNode.insertBefore(js, fjs);
    }
  }

  window.AsyncLoader = AsyncLoader

} (window);
