function initTinyMCE(selector){ 
    tinymce.init({
        document_base_url: base_url,
        selector: "textarea."+selector,
        theme: "modern",
        height: 300,
        language: "hu_HU",
        plugins: [
            "responsivefilemanager advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern e3fm"
        ],
        toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media | forecolor backcolor",
        extended_valid_elements : "iframe[src|style|object|embed|param|width|height|scrolling|marginwidth|marginheight|frameborder]", 
        cleanup_on_startup : true,
        relative_urls : false,
        remove_script_host : false,
        force_br_newlines : false,
        force_p_newlines : true,

        image_advtab: true,
        external_filemanager_path:"/lib/filemanager/",
        filemanager_title:"Filemanager" ,
        filemanager_access_key: rf_key,
        external_plugins: { "filemanager" : "/lib/filemanager/plugin.min.js"},
        theme_advanced_resizing : true,

        entity_encoding : "raw"
     });
}
initTinyMCE('wysiwyg');
