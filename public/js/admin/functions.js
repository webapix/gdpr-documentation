$(document).ready(function(){
	
	$("#nav-collapse").on("click",function() {
		$("body").toggleClass("nav-open");
		$("#content").css("transition","margin 400ms cubic-bezier(0.215, 0.610, 0.355, 1.000)");
		$("#navigation").css("transition","left 400ms cubic-bezier(0.215, 0.610, 0.355, 1.000)");
	})
	
	var width = $(window).width();
	if (width < 993 ) {
		$("#navigation").css("transition","none");
		$("#content").css("transition","none");
		$("body").removeClass("nav-open");
		$("#page-width-select").attr("disabled", true);
		$("#page-nav-select").attr("disabled", true);
	} else {
		$("#page-width-select").attr("disabled", false);
		$("#page-nav-select").attr("disabled", false);
	}

	$(window).resize(function() {
		var width = $(window).width();
		if (width < 993 ) {
			$("body").removeClass("nav-open");
			$("#page-width-select").attr("disabled", true);
			$("#page-nav-select").attr("disabled", true);
		} else {
			$("body").addClass("nav-open");		
			$("#page-width-select").attr("disabled", false);
			$("#page-nav-select").attr("disabled", false);
		}
	});
	
	$("#page-width-select").on("change",function() {
		var wclass = $("#page-width-select option:selected").attr("value");
		$("body").removeClass("width-60").removeClass("width-80").removeClass("width-100");
		$("body").addClass(wclass);
	})
	
	$("#page-nav-select").on("change",function() {
		var sclass = $("#page-nav-select option:selected").attr("value");
		$("body").removeClass("positon-nav-d").removeClass("positon-nav-f");
		$("body").addClass(sclass);
	})
	
	$("#options-default").on("click",function() {
		$("#page-nav-select").prop('selectedIndex',0);
		$("#page-width-select").prop('selectedIndex',0);
		$("body").removeClass("width-60").removeClass("width-80").removeClass("width-100");
		$("body").removeClass("positon-nav-d").removeClass("positon-nav-f");
        saveSettings();
	})
		
	$('a').tooltip();
	$('i').tooltip();
	$('div').tooltip();
	$('button').tooltip();
	$('span').tooltip();
	
	$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
		event.preventDefault();
		return $(this).ekkoLightbox();
	});

	$('.iframe-btn').fancybox({
		'width'		: 860,
		'height'	: 1200,
		'type'		: 'iframe',
		'autoScale'    	: true,
		'autoSize' : false
	});

	$("#delModal").on("show.bs.modal", function(e) {
		var link = $(e.relatedTarget);
		$("#delModal .modal-content").load(link.attr("href"));
	});
    
})
function saveSettings(){
    $.ajax({
            'url':'admin',
            'type':'POST',
            'data':{body:$('#page-width-select').val(),nav:$('#page-nav-select').val(),settings_save:1 }
    });
}