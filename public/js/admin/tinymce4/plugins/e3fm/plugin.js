tinymce.PluginManager.add('e3fm', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('e3fm', {
        text: 'My button',
        icon: false,
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: 'File Manager',  
                resizable : "yes",
                url: 'admin/tinyfm',
               /* body: [
                    {type: 'textbox', name: 'title', label: 'Title'}
                ],   */
                width: 800,
                height: 600,
                buttons: [{
                    text: 'Close',
                    onclick: 'close'
                }],
                onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    editor.insertContent('Title: ' + e.data.title);
                }
            });
        }
    });

});
