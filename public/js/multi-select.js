/**
 *    A formfactory multimodal mezőtípusához tartozó függvények
 **/
 
 
 /**
 *    dynamicSearch - ajax-os kereső
 *    @var text - a keresett szövegrész 
 *    @var query - a tábla neve amiben keresünk 
 *    @var field_schema - a mező neve ami alapján keresünk 
 *    @var ref_id - a mező neve amit mentünk adatbázisba 
 *    @var id - az input mező id-ja, amibe berakjuk a kiválasztott tartalmakat 
 **/
function dynamicSearch(text, url, field_schema, ref_id, id, name, csrf_token) {
    if (text != "") {
        var selected = [];
        $("input[name='"+name+"']").each(function(){
           selected.push($(this).val());
        });
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : csrf_token } }); 
        $.ajax({
                'url': url,
                'type':'POST',
                'data':{
                    text: text,
                    field_schema: field_schema,
                    ref_id: ref_id,
                    id: id,  
                    name: name,  
                    selected: selected  
                },
                success:function(result){
                    $("#multiSelect-"+id+"-list").html(result);
                }
        }); 
    }
    else {
      $("#multiSelect-"+id+"-list").html(" ");  
    }   
}

function addMultiItem(itemId, itemTitle, id, name) {
    $("#multiSelect-"+id+"-list #item-"+itemId).remove();
    $("#multiSelect-"+id+"-values ul").append("<li id='item-"+itemId+"'><a href='javascript:void(0);' onclick='removeMultiItem("+itemId+",\""+itemTitle+"\",\""+id+"\",\""+name+"\")'>"+itemTitle+"</a></li>");
    $("#multiSelect-"+id+"-values").append("<input type='hidden' name='"+name+"' id='input-"+itemId+"' value='"+itemId+"'>");
}
function removeMultiItem(itemId, itemTitle, id, name) {
    $("#multiSelect-"+id+"-values #item-"+itemId).remove();
    $("#multiSelect-"+id+"-values #input-"+itemId).remove();
    $("#multiSelect-"+id+"-list ul").append("<li id='item-"+itemId+"'><a href='javascript:void(0);' onclick='addMultiItem("+itemId+",\""+itemTitle+"\",\""+id+"\",\""+name+"\")'>"+itemTitle+"</a></li>"); 
}