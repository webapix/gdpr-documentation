!function($, window)
{
  $.fn.serializeObject = function()
  {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
      if (o[this.name] !== undefined) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  var ListViewController = function(element, options) {
    this.options = $.extend({}, ListViewController.DEFAULTS, options)

    this.element = $(element)

    this.actionUrl = null
    this.dataCollection = []
    this.modalLoadingTemplate = this.element.find('.js-list-view-modal-loading-template').html()

    this.listContainer = this.element.find('.js-list-view-list-container')
    this.loadListUrl = this.listContainer.data('url')
    this.modal = this.element.find('.js-list-view-modal')
    this.newItemElement = this.element.find('.js-list-view-new-items')
    this.triggerElement = this.element.find('[data-toggle=modal]')

    this.setFormValue()

    this.load()

    this.element
      .on('submit', $.proxy(this.save, this))
      .on('click', '.js-list-view-new-item-delete', $.proxy(this.onNewItemDelete, this))

    this.modal
      .on('show.bs.modal', $.proxy(this.onModalOpen, this))
      .on('hidden.bs.modal', $.proxy(this.onModalHide, this))
      .on('loaded.bs.modal', $.proxy(this.onModalContentLoaded, this))
  }

  ListViewController.prototype.load = function()
  {
    this.listContainer.load(this.loadListUrl, $.proxy(this.onLoadSuccess, this))
  }

  ListViewController.prototype.save = function(e)
  {
    var form = $(e.target)

    e.preventDefault()

    if (!this.actionUrl) return

    if (this.triggerElement.data('id') !== undefined) {

      var formData = form.serialize()

      $.post(this.actionUrl, formData, $.proxy(this.onSaveSuccess, this))
    } else {

      var that = this
        , formData = form.serializeObject()
        , formDataOriginal = $.extend({}, formData)

      delete formData.csrf_token
      delete formData.form_pid

      this.newFormData = formData

      formDataOriginal.new_item = 1

      //$.post(this.actionUrl, {'new_item': this.encode([formData]), 'form_pid': formDataOriginal.form_pid, 'csrf_token': formDataOriginal.csrf_token}, $.proxy(this.onNewItemSuccess, this))
      $.post(this.actionUrl, formDataOriginal, $.proxy(this.onNewItemSuccess, this))

    }
  }

  ListViewController.prototype.encode = function(data)
  {
    var i = 0
      , l = data.length
      , d = data.slice()

    d = d.filter(function(v) {
      return v !== false
    })

    return JSON.stringify(d)
    //return encodeURIComponent(JSON.stringify(data))
  }

  ListViewController.prototype.setFormValue = function()
  {
    this.newItemElement.val(this.encode(this.dataCollection))
  }

  ListViewController.prototype.hideModal = function()
  {
    this.modal.modal('hide')
  }

  ListViewController.prototype.onNewItemSuccess = function(response)
  {
    try {
      response = JSON.parse(response)

      if (response.success) {

        this.dataCollection.push(this.newFormData)

        this.setFormValue()


        var res = $(response.html)

        this.hideModal()

        res.find('.js-list-view-new-item-delete').data('collection-index', this.dataCollection.length-1)

        this.listContainer.append(res)

        this.onLoadSuccess()
      }

    } catch(e) {
      this.modal.find('.modal-content').html(response)
      this.onModalContentLoaded()
    }
  }

  ListViewController.prototype.onNewItemDelete = function(e)
  {
    var self = $(e.currentTarget)

    e.preventDefault()

    this.dataCollection[self.data('collection-index')] = false

    this.setFormValue()

    self.parents(self.data('parent')).remove()
  }

  ListViewController.prototype.onSaveSuccess = function(response)
  {
    try {
      response = JSON.parse(response)

      if (response.success) {

        this.hideModal()
        this.load()
      }

    } catch(e) {
      this.modal.find('.modal-content').html(response)
      this.onModalContentLoaded()
    }
  }

  ListViewController.prototype.onModalOpen = function(e)
  {
    var template = this.modalLoadingTemplate

    this.triggerElement = $(e.relatedTarget)

    this.actionUrl = this.triggerElement.attr('href')

    this.modal.find('.modal-content').html(template)
  }

  ListViewController.prototype.onModalHide = function(e)
  {
    this.actionUrl = null
    this.modal.removeData('bs.modal').find('.modal-content').empty()
    this.triggerElement = null
  }

  ListViewController.prototype.onModalContentLoaded = function(e)
  {
    this.modal.find('.modal-content').wrapInner('<form method="post"></form>')
  }

  ListViewController.prototype.onLoadSuccess = function()
  {
    var self = this

    this.element.find('.js-list-view-item-edit, .js-list-view-item-delete').each(function() {
      $(this).attr('data-target', '#'+self.modal.attr('id'))
    })
  }

  ListViewController.DEFAULTS = {

  }

  function Plugin(option) {
    return this.each(function() {
      var $this = $(this)
        , data = $this.data('listView')
        , options = typeof option === 'object' && option

      if (!data) $this.data('listView', (data = new ListViewController(this, options)))
      if (typeof option === 'string') data[option]()
    })
  }

  var old = $.fn.listView

  $.fn.listView = Plugin
  $.fn.listView.Constructor = ListViewController

  $.fn.listView.noConflict = function () {
    $.fn.listView = old
    return this
  }

  $(window).on('load', function() {
    $('.js-list-view-container').each(function() {
      var self = $(this)

      Plugin.call(self, self.data())
    })
  })

} (jQuery, window);
