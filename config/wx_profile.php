<?php
return [
    'rules' => [
        'name' => 'required',
    ],
    'messages' => [
        'name.required' => 'A név mező kitöltése kötelező!',
    ]
];