<?php
return [
    'rules' => [
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required'
    ],
    'messages' => [
        'name.required' => 'A név mező kitöltése kötelező!',
        'email.required' => 'Az e-mail mező kitöltése kötelező!',
        'email.email' => 'A megadott e-mail cím formátuma nem megfelelő!',
        'message.required' => 'Az üzenet mező kitöltése kötelező!',
    ]
];