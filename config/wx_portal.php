<?php
/**
 * @author Márton-Illés Krisztián <Chris>
 * @copyright Webapix kft. 2017
 */
return [
    'image_cache_lifetime' => env('IMAGE_CACHE_LIFETIME', (60 * 24 * 7)),
    'contact_mail' => env('CONTACT_MAIL', "support@webapix.hu")
];