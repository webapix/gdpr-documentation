<?php

namespace Tests\Browser;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Webapix\Auth\Model\Role;
use Webapix\Auth\Model\User;
use Webapix\Portal\Model\Carousel;

class CarouselSortingTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $language = new \Webapix\Support\Model\Language();
        $language->code = 'hu';
        $language->name = 'magyar';
        $language->active = true;
        $language->default = true;
        $language->save();

        $role = new Role();
        $role->name = 'admin';
        $role->display_name = 'Admin';
        $role->description = 'Admin';
        $role->save();

        $user = new User();
        $user->email = 'test@webapix.hu';
        $user->password = bcrypt('1234');
        $user->name = 'Test user';
        $user->setIsAdmin(true);
        $user->save();

        $carousel = new Carousel();
        $carousel->name = 'teszt';
        $carousel->text = 'teszt';
        $carousel->picture = 'teszt';
        $carousel->start_date = Carbon::now();
        $carousel->end_date = Carbon::now();
        $carousel->target_url = '/';
        $carousel->open_in = 1;
        $carousel->priority = 1;
        $carousel->save();
    }

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        //$this->seed('LanguagesTableSeeder');

        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/carousel/list')
                ->type('email', 'test@webapix.hu')
                ->type('password', '1234')
                ->press('Belépés')
                ->assertSee('Sorrendezés')
                ->assertDontSee('Előnézet');

            $browser->visit('/admin/carousel/new')
                ->assertDontSee('Előnézet');

        });
    }
}
