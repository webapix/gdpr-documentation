<?php
$admins = [
    ['name' => 'gallerymassupload', 'class' => '\Webapix\Portal\Controllers\Admin\GallerymassuploadAdminController'],
    ['name' => 'galleryimages', 'class' => '\Webapix\Portal\Controllers\Admin\GalleryimagesAdminController'],
    ['name' => 'galleries', 'class' => '\Webapix\Portal\Controllers\Admin\GalleriesAdminController'],
    ['name' => 'article', 'class' => 'App\Http\Controllers\Admin\ArticleAdminController'],
    ['name' => 'carousel', 'class' => '\Webapix\Portal\Controllers\Admin\CarouselAdminController'],
    ['name' => 'news', 'class' => '\Webapix\Portal\Controllers\Admin\NewsAdminController'],
    ['name' => 'categories', 'class' => '\Webapix\Portal\Controllers\Admin\NewsCategoryAdminController'],
    ['name' => 'menu', 'class' => '\Webapix\Portal\Controllers\Admin\MenuAdminController'],
    ['name' => 'contact', 'class' => '\Webapix\Portal\Controllers\Admin\ContactAdminController'],
    ['name' => 'newslettersubscription', 'class' => '\Webapix\Portal\Controllers\Admin\NewsletterSubscriptionAdminController'],
    ['name' => 'content', 'class' => '\Webapix\Portal\Controllers\Admin\ContentAdminController']
];

