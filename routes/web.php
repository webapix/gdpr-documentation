<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function () {
    Route::get('article/{article}', 'ArticleController@index')
        ->name('article');
    Route::post('search', 'SearchController@postSearch')->name('postSearch');
    Route::get('search/{query?}', 'SearchController@index')->name('search');
    Route::get('/', 'HomeController@index')->name('home');
});

Auth::routes();


