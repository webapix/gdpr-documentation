<?php
/**
 * Created by PhpStorm.
 * User: Kave
 * Date: 2017. 06. 12.
 * Time: 10:44
 */

namespace App\Http\Controllers\Admin;



use App\Http\Model\Article;

class ArticleAdminController extends \Webapix\Portal\Controllers\Admin\ArticleAdminController
{
    protected $model_class = Article::class;

    /**
     * Editor mezők
     *
     * @param \Webapix\Support\Model\Model $model
     * @return \Webapix\AdminFactory\FieldList
     */
    public function getFieldListForEditor(\Webapix\Support\Model\Model $model)
    {
        $selectElements = [0 => 'Főkategória'];
        Article::where('parent_id', 0)->where('id', '<>', $model->id)->get()->each(function($article) use(&$selectElements) {
            $selectElements[$article->id] = $article->title;
        });

        return new \Webapix\AdminFactory\FieldList([
            ['title', __('articles.admin.title_label'), 'text', []],
            ['parent_id', __('articles.admin.parent_id'), 'select', [], ['setOptions' => $selectElements]],
            ['speakingURL', __('articles.admin.surl'), 'raw', []],
            ['lead', __('articles.admin.lead'), 'tiny', []],
            ['content', __('articles.admin.content'), 'tiny', []],
            ['public', __('articles.admin.public'), 'checkbox'],
            ['active', __('articles.admin.active'), 'checkbox'],
        ]);
    }

}