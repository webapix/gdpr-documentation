<?php
namespace App\Http\Controllers;

use App\Http\Model\Article;

class ArticleController
{
    public function index(Article $article)
    {
        return view('webapix.article.show', [
            'article' => $article
        ]);
    }
}
