<?php
namespace App\Http\Controllers;

use App\Http\Model\Article;
use \Webapix\Portal\Controllers\Frontend\SearchController as PortalSearchController;

class SearchController extends PortalSearchController
{
    public function search($query, $search_in = 0)
    {
        $results = [];

        if (in_array($search_in, ['0', str_slug('Leírások')])) {
            $articles = Article::search($query)->where('active', 1)->paginate(100);
            foreach ($articles as $key => $item) {
                $articles[$key] = [
                    'title' => $item->title,
                    'lead' => $item->lead,
                    'url' => route('article', ['speakingURL' => $item->speakingURL]),
                ];
            }

            $results[__('articles.title')] = $articles;
        }

        return $results;
    }
}
