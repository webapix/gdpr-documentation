<?php

namespace App\Http\Controllers;

use App\Http\Model\Article;

class HomeController extends Controller
{

    public function index()
    {
        $articles = Article::where('parent_id', 0)->paginate(100);

        return view('webapix.index', compact('articles'));
    }
}
