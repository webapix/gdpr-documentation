<?php
namespace App\Http\Controllers\Api;

use App\Http\Model\Article;
use App\Http\Model\Token;
use Illuminate\Http\Request;

class ArticleController
{
    public function show($article_id, Request $request)
    {
        try {
            $article = Article::withoutGlobalScopes()->where('id', $article_id)->firstOrFail();
            $token = Token::where('token', $request->get('token', ''))->first();

            if ($token) {
                return $article;
            }

            return response()->json(['error' => 'Missing or invalid token!']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Invalid article identifier!']);
        }

    }
}
