<?php
namespace App\Http\Model;

use Webapix\Support\Model\Model;

class Token extends Model
{
    protected $table = "tokens";
}