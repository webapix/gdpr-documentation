<?php
namespace App\Http\Model;

use Webapix\Portal\Traits\Publishable;
use \Webapix\Portal\Model\Article as PortalArticle;

class Article extends PortalArticle
{
    use Publishable;

    public function childrens()
    {
        return $this->hasMany(Article::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Article::class, 'parent_id', 'id');
    }

    public function setPublicAttribute($value)
    {
        $this->attributes['public'] = empty($value) ? 0 : $value;
    }
}