<?php

use Illuminate\Database\Seeder;
use Webapix\Auth\Database\Seeds\AdminUserSeeder;
use Webapix\Support\Database\Seeds\LanguagesTableSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);
        $this->call(AdminMenuTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
    }
}
