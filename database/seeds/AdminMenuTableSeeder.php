<?php

use Illuminate\Database\Seeder;

class AdminMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu_items = collect([
            [
                'title' => 'Felhasználók',
                'url' => 'admin/users/list',
                'active' => 1
            ],
            [
                'title' => 'Menü',
                'url' => 'admin/menu/list',
                'active' => 1
            ],
            [
                'title' => 'Oldalak',
                'url' => 'admin/article/list',
                'active' => 1
            ],
        ]);

        $menu_items->each(function ($item) {
            $am = new \Webapix\Support\Model\AdminMenu();
            $am->title = $item['title'];
            $am->url = str_replace('http://localhost/', '', $item['url']);
            $am->active = $item['active'];
            $am->priority = 0;
            $am->save();
        });
    }
}
