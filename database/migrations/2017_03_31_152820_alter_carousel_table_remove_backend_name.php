<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCarouselTableRemoveBackendName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carousel', function(Blueprint $table){
            $table->dropColumn(['backend_name']);
        });

        Schema::table('carousel', function(Blueprint $table){
            $table->renameColumn('frontend_name', 'name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carousel', function(Blueprint $table){
           $table->renameColumn('name', 'frontend_name');
        });

        Schema::table('carousel', function(Blueprint $table){
            $table->string('backend_name', 255)->nullable();
        });
    }
}
