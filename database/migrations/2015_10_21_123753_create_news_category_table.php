<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->unsignedInteger('parent')->nullable();
            $table->boolean('active');
            $table->boolean('del')->default(false);
            $table->integer('szekcio_id');
            $table->index(['parent', 'active']);
            $table->timestamps();
            $table->foreign('parent')->references('id')->on('news_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_category');
    }
}
