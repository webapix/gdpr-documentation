<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',200);
            $table->string('speakingURL',200);
            $table->text('lead');
            $table->string('lead_picture',255);
            $table->text('content');
            $table->string('author',200);
            $table->integer('priority');
            $table->timestamp('date_from');
            $table->boolean('active');
            $table->string('meta_title',200);
            $table->string('meta_description',200);
            $table->string('meta_keywords',200);
            $table->timestamps();
            $table->index(['priority', 'date_from']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
