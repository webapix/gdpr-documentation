<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CrateAdminMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webapix_admin_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('url');
            $table->unsignedInteger('parent')->nullable();
            $table->boolean('active');
            $table->integer('priority')->index();
            $table->index(['parent', 'active']);
            $table->timestamps();
            $table->foreign('parent')->references('id')->on('webapix_admin_menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('webapix_admin_menu');
    }
}
