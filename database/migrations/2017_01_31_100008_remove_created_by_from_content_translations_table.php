<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveCreatedByFromContentTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webapix_content_translations', function (Blueprint $table) {
            $table->dropColumn(['created_by']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webapix_content_translations', function (Blueprint $table) {
            $table->unsignedInteger('created_by')->default(0);
        });
    }
}
