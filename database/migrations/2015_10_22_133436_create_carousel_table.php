<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarouselTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carousel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('backend_name', 255)->default('');
            $table->string('frontend_name', 255)->default('');
            $table->text('text');
            $table->string('picture', 255);
            $table->dateTime('start_date')->index();
            $table->dateTime('end_date')->index();
            $table->string('target_url', 255);
            $table->tinyInteger('open_in');
            $table->integer('priority')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carousel');
    }
}
