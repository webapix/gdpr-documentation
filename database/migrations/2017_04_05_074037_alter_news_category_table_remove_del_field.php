<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNewsCategoryTableRemoveDelField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_category', function(Blueprint $table) {
            $table->dropColumn('del');
            $table->dropForeign(['parent']);
        });

        Schema::table('news_category', function(Blueprint $table){
            $table->renameColumn('parent', 'parent_id');
        });

        Schema::table('news_category', function(Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')
                ->on('news_category')
                ->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_category', function(Blueprint $table){
            $table->dropForeign(['parent_id']);
        });

        Schema::table('news_category', function(Blueprint $table){
            $table->boolean('del')->default(false);
        });

        Schema::table('news_category', function(Blueprint $table){
            $table->renameColumn('parent_id', 'parent');
        });

        Schema::table('news_category', function(Blueprint $table){
            $table->foreign('parent')
                ->references('id')
                ->on('news_category');
        });
    }
}
