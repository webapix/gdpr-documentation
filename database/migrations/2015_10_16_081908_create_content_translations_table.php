<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webapix_content_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 64);
            $table->string('field_name', 64);
            $table->text('data');
            $table->string('lang', 8);
            $table->unsignedInteger('created_by')->default(0);
            $table->timestamps();
            $table->index(['uuid', 'lang', 'field_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('webapix_content_translations');
    }
}
