<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsInCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_in_category', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('news_id');
            $table->unsignedInteger('category_id');
            $table->timestamps();
            $table->index(['news_id']);
            $table->index(['category_id']);
            $table->foreign('news_id')->references('id')->on('news');
            $table->foreign('category_id')->references('id')->on('news_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_in_category');
    }
}
