<?php

return array (
  'logout' => 'Kilépés',
  'login' => 'Belépés',
  'register' => 'Regisztráció',
  'failed' => 'These credentials do not match our records.',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
);
