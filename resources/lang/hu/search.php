<?php

return array (
  'results_title' => 'Találatok a :query kifejezésre',
  'details' => 'Tovább &raquo;',
  'no_results' => 'Nincs találat',
  'further_results' => 'További találatok »',
  'title' => 'Keresés',
  'label' => 'Keresés',
  'placeholder' => 'Keresés...',
);
