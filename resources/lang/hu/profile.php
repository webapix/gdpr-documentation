<?php

return array (
  'title' => 'Profil',
  'change_success' => 'Sikeres módosítás',
  'my_info' => 'Adataim',
  'name' => 
  array (
    'required' => 'A név mező kitöltése kötelező!',
  ),
  'submit' => 'Módosítás',
  'change_password' => 'Jelszó módosítása',
  'password' => 
  array (
    'min' => 'A jelszónak minimum :min karakterből kell állnia!',
    'mismatch' => 'A jelszó és a jelszó megerősítő mezőnek azonosnak kell lennie',
  ),
  'confirm_password' => 'Jelszó újra',
  'password_required' => 'A jelszó mező kitöltése kötelező!',
);
