<?php

return array (
  '404_title' => 'Hiba lépett fel',
  '404_subtitle' => '404 - Az oldal nem található',
  'home_link' => 'A főoldalt :link kattintva érheted el',
  '403_title' => 'Hiba lépett fel',
  '403_subtitle' => '403 - Hozzáférés megtagadva',
  'generic_title' => 'Hiba lépett fel',
  'generic_subtitle' => 'Kérjük, próbálkozz újra később!',
  '503_title' => 'Hiba lépett fel',
  '503_subtitle' => '503 - A szolgáltatás pillanatnyilag nem elérhető',
);
