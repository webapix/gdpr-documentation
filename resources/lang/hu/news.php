<?php

return array (
  'title' => 'Hírek',
  'no_results' => 'Nincs találat',
  'details' => 'Tovább &raquo;',
  'admin' => 
  array (
    'title' => 'Hírek',
    'title_label' => 'Cím',
    'surl' => 'Beszédes URL',
    'lead' => 'Bevezető',
    'content' => 'Tartalom',
    'lead_picture' => 'Borítókép',
    'author' => 'Szerző',
    'categories' => 'Kategóriák',
    'date_from' => 'Mikortól aktív',
    'active' => 'Aktív',
    'public' => 'Publikus',
    'meta_title' => 'Meta cím',
    'meta_desc' => 'Meta leírás',
    'created_at' => 'Létrehozva',
  ),
);
