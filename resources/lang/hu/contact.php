<?php

return array (
  'title' => 'Kapcsolat',
  'success_msg' => 'Sikeres kapcsolatfelvétel!',
  'error' => 
  array (
    'name_required' => 'Név megadása kötelező!',
    'email_required' => 'E-mail megadása kötelező!',
    'invalid_email' => 'Érvénytelen e-mail cím!',
    'message_required' => 'Üzenet megadása kötelező!',
  ),
  'phone' => 'Telefonszám',
  'message' => 'Üzenet',
  'submit' => 'Küldés',
  'email' => 
  array (
    'subject' => 'Kapcsolatfelvétel',
  ),
  'admin' => 
  array (
    'name' => 'Név',
    'email' => 'E-mail',
    'phone' => 'Telefonszám',
    'message' => 'Üzenet',
  ),
);
