<?php

return array (
  'title' => 'Regisztráció',
  'name' => 'Név',
  'email' => 'E-mail',
  'password' => 'Jelszó',
  'confirm_password' => 'Jelszó újra',
  'submit' => 'Regisztráció',
);
