<?php

return array (
  'previous' => 'Előző',
  'next' => 'Következő',
  'admin' => 
  array (
    'title' => 'Forgók',
    'name' => 'Név',
    'text' => 'Szöveg',
    'picture' => 'Kép',
    'start_date' => 'Kezdő dátum',
    'end_date' => 'Vége dátum',
    'target_url' => 'Cél URL',
    'open_in' => 'Megnyitás',
  ),
);
