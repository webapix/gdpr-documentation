<?php

return array (
  'title' => 'Galériák',
  'previous' => 'Előző',
  'next' => 'Következő',
  'details' => 'Tovább &raquo;',
  'admin' => 
  array (
    'title' => 'Galériák',
    'description' => 'Leírás',
    'public' => 'Publikus',
    'cover_image' => 'Borítókép',
    'images' => 'Képek',
  ),
);
