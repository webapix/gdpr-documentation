<?php

return array (
  'title' => 'Hírlevél',
  'placeholder' => 'E-mail',
  'email' => 
  array (
    'required' => 'Az e-mail mező kitöltése kötelező!',
    'invalid' => 'Érvénytelen e-mail cím!',
    'already_exists' => 'Ez az e-mail cím már szerepel az adatbázisunkban!',
  ),
  'submit' => 'Feliratkozás',
  'admin' => 
  array (
    'title' => 'Hírlevél feliratkozók',
    'email' => 'E-mail',
  ),
  'success' => 'Sikeres hírlevélfeliratkozás!',
);
