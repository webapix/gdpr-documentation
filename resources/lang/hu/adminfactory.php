<?php
/**
 * @author Márton-Illés Krisztián <Chris>
 * @copyright Webapix kft. 2017
 */
return array(
    'translations' => [
        'import' => 'Adatbázis frissítése éles fordítások alapján',
        'export' => 'Fordítások élesítése',
        'find' => 'Új fordítandók keresése fájlokban'
    ]
);