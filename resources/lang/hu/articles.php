<?php

return array (
  'admin' => 
  array (
    'title' => 'Statikus tartalmak',
    'parent_id' => 'Kategória',
    'name' => 'Cím',
    'content' => 'Tartalom',
    'created_at' => 'Létrehozva',
    'title_label' => 'Cím',
    'surl' => 'Beszédes URL',
    'lead' => 'Bevezető',
    'active' => 'Aktív',
    'public' => 'Publikus?',
  ),
  'title' => 'Statikus tartalmak',
);
