<?php

return array (
  'admin' => 
  array (
    'title' => 'Képek tömeges feltöltése',
    'images_in_zip' => 'Képek ZIP-ben',
    'image' => 'Kép',
    'gallery_id' => 'Galéria',
    'created_at' => 'Létrehozva',
    'updated_at' => 'Módosítva',
  ),
);
