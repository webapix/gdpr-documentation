<?php

return array (
  'admin' => 
  array (
    'title' => 'Articles',
    'name' => 'Title',
    'content' => 'Contents',
    'created_at' => 'Created at',
    'title_label' => 'Title',
    'surl' => 'Speaking URL',
    'lead' => 'Lead',
    'active' => 'Active',
  ),
  'title' => 'Articles',
);
