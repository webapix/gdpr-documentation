<?php

return array (
  'title' => 'Newsletter',
  'email' => 
  array (
    'required' => 'The e-mail field is required!',
    'invalid' => 'Invalid e-mail!',
    'already_exists' => 'This e-mail address is already used!',
  ),
  'submit' => 'Subscribe',
  'admin' => 
  array (
    'title' => 'Subscribers',
    'email' => 'E-mail',
  ),
  'success' => 'Successfuly subscribed to newsletter!',
);
