<?php

return array (
  'title' => 'Profile',
  'change_success' => 'Changes saved successfuly!',
  'my_info' => 'My info',
  'name' => 
  array (
    'required' => 'The name field is required!',
  ),
  'submit' => 'Submit',
  'change_password' => 'Change password',
  'password' => 
  array (
    'min' => 'The password should be at least :min characters long!',
    'mismatch' => 'Password mismatch!',
  ),
  'confirm_password' => 'Confirm password',
  'password_required' => 'The password field is required!',
);
