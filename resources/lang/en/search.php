<?php

return array (
  'results_title' => 'Results for ":query"',
  'details' => 'Details &raquo;',
  'no_results' => 'No results',
  'further_results' => 'More results »',
  'title' => 'Search',
  'label' => 'Search',
  'placeholder' => 'Search...',
);
