<?php

return array (
  'title' => 'Registration',
  'name' => 'Name',
  'email' => 'E-mail',
  'password' => 'Password',
  'confirm_password' => 'Confirm password',
  'submit' => 'Register',
);
