<?php

return array (
  'title' => 'Galleries',
  'previous' => 'Previous',
  'next' => 'Next',
  'details' => 'Details &raquo;',
  'admin' => 
  array (
    'title' => 'Galleries',
    'description' => 'Description',
    'public' => 'Public',
    'cover_image' => 'Cover image',
    'images' => 'Images',
  ),
);
