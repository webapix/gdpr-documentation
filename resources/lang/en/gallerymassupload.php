<?php

return array (
  'admin' => 
  array (
    'title' => 'Mass upload images',
    'images_in_zip' => 'Images in ZIP',
    'image' => 'Image',
    'gallery_id' => 'Gallery',
    'created_at' => 'Created at',
    'updated_at' => 'Updated at',
  ),
);
