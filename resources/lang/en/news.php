<?php

return array (
  'title' => 'News',
  'no_results' => 'No results',
  'details' => 'Details &raquo;',
  'admin' => 
  array (
    'title' => 'News',
    'title_label' => 'Title',
    'surl' => 'Speaking URL',
    'lead' => 'Lead',
    'content' => 'Content',
    'lead_picture' => 'Lead picture',
    'author' => 'Author',
    'categories' => 'Categories',
    'date_from' => 'Active from',
    'active' => 'Active',
    'public' => 'Public',
    'meta_title' => 'Meta title',
    'meta_desc' => 'Meta description',
    'created_at' => 'Created_at',
  ),
);
