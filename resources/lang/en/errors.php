<?php

return array (
  '404_title' => 'An error occured!',
  '404_subtitle' => '404 - Not found!',
  'home_link' => 'You can go back to the main page by clicking <a href=":href">here</a>',
  '403_title' => 'An error occured!',
  '403_subtitle' => '403 - Permission denied!',
  'generic_title' => 'An error occured!',
  'generic_subtitle' => 'Please try again later!',
  '503_title' => 'An error occured!',
  '503_subtitle' => '503 - Service temporarily unavailable',
);
