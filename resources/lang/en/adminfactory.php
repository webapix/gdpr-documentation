<?php
/**
 * @author Márton-Illés Krisztián <Chris>
 * @copyright Webapix kft. 2017
 */
return array(
    'translations' => [
        'import' => 'Import translations for editing',
        'export' => 'Export translations into system',
        'find' => 'Find new translatable items in files'
    ]
);