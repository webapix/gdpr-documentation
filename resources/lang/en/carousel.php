<?php

return array (
  'previous' => 'Previous',
  'next' => 'Next',
  'admin' => 
  array (
    'title' => 'Carousels',
    'name' => 'Name',
    'text' => 'Text',
    'picture' => 'Picture',
    'start_date' => 'Starts at',
    'end_date' => 'Ends at',
    'target_url' => 'Target URL',
    'open_in' => 'Open in',
  ),
);
