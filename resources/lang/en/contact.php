<?php

return array (
  'title' => 'Contact',
  'success_msg' => 'Message sent successfuly!',
  'error' => 
  array (
    'name_required' => 'The name field is required!',
    'email_required' => 'The e-mail field is required!',
    'invalid_email' => 'Invalid e-mail address!',
    'message_required' => 'The message field is required!',
  ),
  'phone' => 'Phone',
  'message' => 'Message',
  'submit' => 'Send',
  'email' => 
  array (
    'subject' => 'New contact',
  ),
  'admin' => 
  array (
    'name' => 'Name',
    'email' => 'E-mail',
    'phone' => 'Phone',
    'message' => 'Message',
  ),
);
