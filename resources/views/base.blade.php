<!DOCTYPE html>
<html lang="hu">
<head>
    <base href="{{ config('app.basehref', '/') }}"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>{{ $metatitle or ''}}</title>
    <meta name="description" content="{{ $metadesc or '' }}"/>
    <meta name="keywords" content="{{ $keywords or '' }}"/>
    <meta property="og:title" content="{{ $metatitle  or ''}}"/>
    <meta property="og:description" content="{{ $metadesc or '' }}"/>

    @yield('header')
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/description.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/carousel.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/admin/colorpicker/css/bootstrap-colorpicker.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('js/dropzone/dropzone.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('js/dropzone/basic.css')}}"/>

    <link rel="stylesheet" href="{{asset('css/developer.css')}}"/>

    {!! Html::script('js/jquery.js') !!}
    <script type="text/javascript" src="{{ asset('js/dropzone/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/filemanager.js') }}"></script>
    <script>Dropzone.autoDiscover = false;</script>
    <script type="text/javascript" src="{{ asset('js/multi-select.js') }}"></script>
    <!-- Bootstrap inicialize -->
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <!--[if lt IE 7 ]>
    <script src="{{ asset('js/png_hack/dd_belatedpng.js') }}" type="text/javascript"></script>
    <script type="text/javascript">DD_belatedPNG.fix("img, .png_bg");</script>
    <![endif]-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="{{asset('js/async-loader.js')}}"></script>

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{asset('js/admin/bootstrap.js')}}"></script>

    <!-- Date/DateTimepicker inicialize -->
    <script src="{{asset('js/admin/jqueryui/jquery-ui-timepicker-addon.js')}}"></script>
    <!-- Bootstrap ColorPicker inicialize -->
    <script src="{{asset('js/admin/bootstrap-colorpicker.min.js')}}"></script>

    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.js') }}"></script>
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    {!! $analytics or '' !!}
</head>

<!--[if lt IE 7 ]>
<body class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>
<body class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>
<body class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<body class="no-js"><!--<![endif]-->


<header id="header">
    <div class="page-inner">

    </div> <!-- .page-inner -->
</header> <!-- #header -->

<div id="page">

    <div id="sidebar">

    </div> <!-- #sidebar -->

    <div id="content">
        @yield('content')

    </div> <!-- #content -->

    <div class="clear"></div>

    <footer id="footer">
        © Minden jog fenntartva!
    </footer> <!-- #footer -->

</div> <!-- #page -->

</body>
</html>
