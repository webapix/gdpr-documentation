<div class="panel">
    <h4 class="panel-heading">{{ __('galleries.title') }}</h4>

    <div class="panel-body">
        <div class="row">
            @foreach($galleries as $gallery)
                <div class="col-xs-12 col-sm-4 col-lg-12" style="margin-bottom: 22px;">
                    <div style="margin-bottom: 5px;">
                        <a href="{{ route('gallery.show', $gallery) }}" class="thumbnail"
                           style="display: block; margin-bottom: 5px; padding-top: 56.26%; background-repeat: no-repeat; background-position: center center; background-size: cover; background-image: url('{{ getThumbnail($gallery->cover_image, 705, 396) }}');"></a>
                        <a href="{{ route('gallery.show', $gallery) }}">{{ $gallery->title }}</a>
                    </div>
                    <a class="btn btn-sm btn-primary" href="{{ route('gallery.show', $gallery) }}"
                       role="button">{{ __('galleries.details') }}</a>
                </div>
            @endforeach
        </div>
    </div> <!-- /.panel-body -->
</div><!-- /.panel -->
