@extends('webapix.base', [
    'metatitle' => $gallery->title,
    'metadesc' => $gallery->getMetaDescription(),
    'metaimage' => empty($gallery->cover_image) ? '' : asset($gallery->cover_image),
    'metaurl' => route('gallery.show', $gallery->speakingURL)
])

@section('main')
    @include('webapix.partials.breadcrumb', ['elements' => [
     route('home') => __('home.title'),
     route('gallery.index') => __('galleries.title'),
     route('gallery.show', $gallery) => $gallery->title
 ]])
    <div class="panel">
        <div class="panel-heading">
            <h4>{{ $gallery->title }}</h4>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    {!! $gallery->description !!}
                </div>
            </div>

            <div class="row">
                @foreach($gallery->getImages() as $i => $image)
                    <div class="col-xs-12 col-sm-4" data-toggle="modal" data-target="#modal-gallery"
                         style="margin-bottom: 22px;">
                        <a href="javascript:;" class="thumbnail"
                           style="display: block; margin-bottom: 5px; padding-top: 56.26%; background-repeat: no-repeat; background-position: center center; background-size: cover; background-image: url('{{ getThumbnail($image->image, 705, 396) }}');"
                           data-target="#carousel-gallery" data-slide-to="{{ $i }}"></a>
                        {{ $image->title }}
                    </div>
                @endforeach
            </div>
            <div class="modal fade" id="modal-gallery" tabindex="-1" role="dialog" aria-hidden="true">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <a class="left carousel-control" href="#carousel-gallery" role="button"
                   data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"> @lang('galleries.previous') </span>
                </a>
                <a class="right carousel-control" href="#carousel-gallery" role="button"
                   data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"> @lang('galleries.next') </span>
                </a>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">

                            <div id="carousel-gallery" class="carousel slide" data-ride="carousel" data-interval="false"
                                 data-wrap="true">
                                <div class="carousel-inner" role="listbox">
                                    @foreach($gallery->getImages() as $image)
                                        <div class="item @if( $loop->index == 0 ) active @endif">
                                            <img src="{{ $image->image }}" alt="">
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /.panel-body -->
        <div class="panel-footer">
            <a class="btn btn-primary" href="https://facebook.com/sharer/sharer.php?u={{ url()->current() }}"
               target="_blank" aria-label="Facebook">
                Facebook
            </a>
            <a class="btn btn-primary"
               href="https://twitter.com/intent/tweet/?text={{ urlencode($gallery->title) }}&amp;url={{ url()->current() }}"
               target="_blank" aria-label="Twitter">
                Twitter
            </a>
            <a class="btn btn-primary" href="https://plus.google.com/share?url={{ url()->current() }}" target="_blank"
               aria-label="Google+">
                Google+
            </a>
        </div> <!-- /.panel-footer -->
    </div><!-- /.panel -->
@endsection
