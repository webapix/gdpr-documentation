<div class="panel panel-gallery">
    <h4 class="panel-heading"> @lang('galleries.title') </h4>

    <div class="panel-body">
        <div class="row">
            @foreach($galleries as $gallery)
                <div class="col-xs-12 col-sm-4" style="margin-bottom: 22px;">
                    <a class="thumbnail" href="{{ route('gallery.show', $gallery) }}"
                       style="display: block; margin-bottom: 5px; padding-top: 56.26%; background-repeat: no-repeat; background-position: center center; background-size: cover; background-image: url('{{ getThumbnail($gallery->cover_image, 705, 396) }}');"></a>
                    <a href="{{ route('gallery.show', $gallery) }}">{{ $gallery->title }}</a>
                </div>
            @endforeach
        </div> <!-- /.row -->
    </div> <!-- /.panel-body -->
</div><!-- /.panel -->
