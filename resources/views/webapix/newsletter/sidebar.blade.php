<div class="panel panel-newsletter">
    <h4 class="panel-heading">{{ __('newsletter.title') }}</h4>
    <div class="panel-body">
        @if(isset($errors) && $errors->has('newsleter_email'))
            <div class="alert bg-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @elseif(Session::has('success'))
            <div>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif

        {{ Form::open(array('url' => route('newsletter_subscription_create'))) }}

        <?php /*
            <div class="form-group">
                {{ Form::label('name', 'Név', array(
                    'class' => 'sr-only',
                )) }}
                {{ Form::text('name', null, array(
                    'class' => 'form-control',
                    'placeholder' => 'Név'
                )) }}
            </div>
            */ ?>

        <div class="form-group">
            {{ Form::label('newsleter_email', __('newsletter.email.required'), array(
                'class' => 'sr-only',
            )) }}
            {{ Form::email('newsleter_email', null, array(
                'class' => 'form-control',
                'placeholder' => __('newsletter.placeholder')
            )) }}
        </div>

        <div class="form-group">
            {{ Form::submit(__('newsletter.submit'), array(
                'class' => 'btn btn-primary btn-sm'
            )) }}
        </div>

        {{ Form::close() }}

    </div> <!-- /.panel-body -->
</div> <!-- /.panel-newsletter -->
