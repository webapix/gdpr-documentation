<!doctype html>
<html class="no-js no-svg no-touch" lang="{{ config('app.locale') }}">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <base href="{{ config('app.basehref', '/') }}"/>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="dns-prefetch" href="https://www.google-analytics.com">

    <title>{{ $metatitle or '' }}</title>

    <meta name="description" property="og:description" content="{{ $metadesc or '' }}">
    <meta property="og:title" content="{{ $metatitle or '' }}">
    <meta property="og:image" content="">
    <meta property="og:url" content="">

    @yield('meta')

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!--[if IE]>
    <script>
        var e = ('abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark menu menuitem meter nav output progress section summary template time video').split(' ');

        for (var i = 0; i < e.length; i++) {
            document.createElement(e[i]);
        }
    </script>
    <![endif]-->

    @include('webapix.partials.favicon', array('theme' => '#f36f21', 'mask' => '#f36f21'))

	{{--
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    --}}
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

</head>

<body itemscope itemtype="http://schema.org/WebSite">

<div class="document" role="document" itemprop="hasPart" itemscope itemtype="http://schema.org/WebPage">
    <div class="container">

        <div class="row clearfix">
            <div class="col-xs-12 header-block">

                <header class="banner" itemscope itemtype="http://schema.org/WPHeader">
                    <div class="banner-menubar" style="position: absolute; right: 20px; top: 10px;">
                        @if(Auth::user())
                            <a href="{{ route('logout') }}">{{ __('wxauth.logout') }}</a>
                        @else
                            <a href="{{ route('login') }}">{{ __('wxauth.login') }}</a>{{-- |
                            <a href="{{ route('register') }}">{{ __('wxauth.register') }}</a>--}}
                        @endif
                    </div> <!-- /.banner-menubar -->
                    <div class="page-header">
                        <div class="banner-img clearfix">
                            <a rel="home" href="{{ URL::route('home') }}" title="GDPR">
                                <img src="{{ asset('images/gdpr/GDPR.png') }}" alt="GDPR"
                                     class="img-responsive">
                            </a>
                        </div> <!-- /.banner-img -->
                    </div>
                </header> <!-- /.banner -->

            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->

		{{--
        <div class="row">
            <div class="col-xs-12">

                <nav class="navbar" itemscope itemtype="http://schema.org/SiteNavigationElement">

                    <div class="navbar-header pull-left">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="modal"
                                data-target="#modal-navigation" aria-expanded="false">
                            <span class="glyphicon glyphicon-menu-hamburger"></span>
                            <span class="navigation-button-caption"> @lang('home.menu') </span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse">
                        @include('webapix.partials.navigation')
                    </div>

                    <div class="modal fade" id="modal-navigation" tabindex="-1" role="dialog" aria-hidden="true">
                        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('main.close')  }}"
                                style="width: 40px; outline: none !important; color: #fff; font-size: 40px; line-height: 40px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="modal-dialog"
                             style="-webkit-transform: translate(0, 60px); transform: translate(0, 60px);">
                            <div class="modal-body">
                                @include('webapix.partials.navigation')
                            </div>
                        </div>
                    </div>

                </nav> <!-- /.navigation -->

            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->
		--}}
        
        <div class="row content-main-block">
            <div class="col-lg-8">

                <main class="main" itemscope itemprop="mainContentOfPage">

                    @yield('main')

                </main> <!-- /.main -->

            </div> <!-- /.col-lg-8 -->
            <div class="col-lg-4">

                <aside class="complementary" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">

                    @include('webapix.search.sidebar')
                    
                    <div class="panel panel-newsletter">
                        <h4 class="panel-heading">Ügyfélszolgálat</h4>
                        <div class="panel-body">
                            
                    		+36-70-949-2665,<br>
                            +36-70-949-BOOK<br>
                            <a href="mailto:ugyfelszolgalat@book24.hu">ugyfelszolgalat@book24.hu</a>
                        </div> <!-- /.panel-body -->
                    </div> <!-- /.panel-newsletter -->
                    
                    {{--
                    @include('webapix.newsletter.sidebar')
                    @include('webapix.galleries.sidebar')
                    @include('webapix.news.sidebar', ['count' => 1])
                    --}}


                </aside> <!-- /.complimentary -->

            </div> <!-- /.col-lg-4 -->
        </div> <!-- /.row -->

        <div class="footer-block clearfix">
            <div class="col-xs-12">

                <footer class="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <p class="text-left">
                                    <time>{{ date('Y') }}</time> Copyright, WEBAPIX Kft.<br />
                                    {{-- <a href="{{ URL::route('sitemap') }}"> @lang('main.sitemap') </a> --}}
                            </p> 
                        </div>
                        <div class="col-sm-6 col-xs-12">
                        	<p class="text-right">
                            	<a href="http://webapix.hu" target="_blank"><img src="{{ asset('images/webapix.png') }}" alt="WEBAPIX Kft." width="127" /></a>
                            </p>
                        </div>
                    </div>
                </footer> <!-- /.contentinfo -->

            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->


    </div> <!-- /.container-fluid -->
</div> <!-- /.document -->

<script src="{{ asset('js/vendor.js') }}"></script>
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}

{{--
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
--}}

<div id="fb-root"></div>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    window.cookieconsent_options = {
        "message": "Weboldalunk a jobb felhasználói élmény biztosítása érdekében sütiket használ. A Weboldal használatával Ön beleegyezik az ilyen adatfájlok fogadásába és elfogadja az adat és süti - kezelésre vonatkozó irányelveket.",
        "dismiss": "Értem",
        "learnMore": "További információk",
        "link": "",
        "theme": "dark-bottom"
    };
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
@stack('scripts')
<!-- Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="imageModalLabel"></h4>
            </div>
            <div class="modal-body">
                <img src="" alt="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Bezárás</button>
            </div>
        </div>
    </div>
</div>

<script>
        $('#imageModal').on('show.bs.modal', function (event) {
            var link = $(event.relatedTarget);
            var modal = $(this)
            modal.find('.modal-body img').attr('src', link.data('src'));
        });
</script>
</body>

</html>
