<div class="panel panel-news">
    <h4 class="panel-heading">{{ __('news.title') }}</h4>

    <div class="panel-body">
        <div class="row">
            @if( !empty($news) )
                @foreach($news as $item)
                    <div class="col-xs-12 col-sm-4 col-lg-12" style="margin-bottom: 22px;">
                        <div style="margin-bottom: 5px;">
                            <a href="{{ URL::route('news_details', ['speakingURL' => $item->speakingURL]) }}"
                               class="thumbnail"
                               style="display: block; margin-bottom: 5px; padding-top: 56.26%; background-repeat: no-repeat; background-position: center center; background-size: cover; background-image: url('thumbnails/705/396/{{ $item->lead_picture }}');"></a>
                            <a href="{{ URL::route('news_details', ['speakingURL' => $item->speakingURL]) }}">{{ $item->title }}</a>
                        </div>
                        <a class="btn btn-sm btn-primary"
                           href="{{ URL::route('news_details', ['speakingURL' => $item->speakingURL]) }}"
                           role="button">{{ __('news.details') }}</a>
                    </div>
                @endforeach
            @endif
        </div>
    </div> <!-- /.panel-body -->
</div> <!-- /.panel-news -->
