@extends('webapix.base', [
    'metatitle' => 'Hírek',
    'metaurl' => route('news')
])

@section('main')

    @if (isset($category))
        @include('webapix.partials.breadcrumb', ['elements' => [
            route('home') => __('home.title'),
            route('news') => __('news.title'),
            route('news_category', ['speakingURL' => $category->speakingURL]) => $category->name,
        ]])
    @else
        @include('webapix.partials.breadcrumb', ['elements' => [
            route('home') => __('home.title'),
            route('news') => __('news.title'),
        ]])
    @endif


    @if( !empty($news) )
        @foreach($news as $item)
            <div class="panel">
                <div class="panel-heading">
                    <h4>
                        <a href="{{ route('news_details', ['speakingURL' => $item->speakingURL]) }}">{{ $item->title }}</a>
                    </h4>
                    <small>{{ $item->date_from }}
                        @if( !empty($item->author) )
                            |
                            @if(is_array($item->author))
                                @foreach( $item->author as $author )
                                    {{ $author }}
                                @endforeach
                            @else
                                {{ $item->author }}
                            @endif
                        @endif
                    </small>
                </div> <!-- /.panel-heading -->

                <div class="panel-body" style="padding-bottom: 0;">
                    <div class="plain-text"><strong>{!! $item->lead !!}</strong></div>

                    <div class="row">
                        <a href="{{ route('news_details', ['speakingURL' => $item->speakingURL]) }}">
                            <img class="img-responsive" src="{{ getThumbnail($item->lead_picture, 940) }}"
                                 style="width: 100%" alt="{{ $item->title }}"/>
                        </a>
                    </div>
                </div> <!-- /.panel-body -->

                <div class="panel-footer">
                    <a class="btn btn-sm btn-primary"
                       href="{{ route('news_details', ['speakingURL' => $item->speakingURL]) }}"
                       role="button">{{ __('Tovább &raquo;') }}</a>
                </div> <!-- /.panel-footer -->
            </div><!-- /.panel -->
        @endforeach

        {{ $news->links() }}
    @else
        <div class="panel">
            <div class="panel-heading">
                <h4> @lang('news.no_results') </h4>
            </div>
        </div>
    @endif

@endsection

