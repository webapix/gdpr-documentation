@if( !empty($news) )
    @foreach($news as $item)
        <div class="panel">
            <div class="panel-heading">
                <h4>
                    <a href="{{ URL::route('news_details', ['speakingURL' => $item->speakingURL]) }}">{{ $item->title }}</a>
                </h4>
                <small>{{ $item->date_from }}
                    @if( !empty($item->author) )
                        |
                        @if(is_array($item->author))
                            @foreach( $item->author as $author )
                                <a href="{{ URL::route('news') }}?author={{ $author }}">{{ $author }}</a>
                            @endforeach
                        @else
                            {{ $item->author }}
                        @endif
                    @endif
                </small>
            </div> <!-- /.panel-heading -->

            <div class="panel-body" style="padding-bottom: 0;">
                <div class="plain-text"><strong>{!! $item->lead !!}</strong></div>

                <div class="row">
                    <a href="{{ URL::route('news_details', ['speakingURL' => $item->speakingURL]) }}">
                        <img class="img-responsive" src="{{ getThumbnail($item->lead_picture, 940) }}"
                             style="width: 100%" alt="{{ $item->title }}"/>
                    </a>
                </div>
            </div> <!-- /.panel-body -->

            <div class="panel-footer">
                <a class="btn btn-sm btn-primary"
                   href="{{ URL::route('news_details', ['speakingURL' => $item->speakingURL]) }}"
                   role="button">{{ __('news.details') }}</a>
            </div> <!-- /.panel-footer -->
        </div><!-- /.panel -->
    @endforeach
@endif
