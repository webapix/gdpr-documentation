@extends('webapix.base',[
    'metatitle' => empty($item->meta_title) ? $item->title : $item->meta_title,
    'metadesc' => $item->getMetaDescription(),
    'metaimage' => empty($item->lead_picture) ? '' : asset($item->lead_picture),
    'metaurl' => route('news_details', $item->speakingURL)
])

@section('meta')

    <title>{{ $item->meta_title }}</title>

    @if( !empty($item->meta_description))
        <meta name="description" content="{{ $item->meta_description }}"/> @endif
    @if( !empty($item->meta_keywords))
        <meta name="keywords" content="{{ $item->meta_keywords }}"/> @endif
    @if( !empty($item->meta_title))
        <meta property="title" content="{{ $item->meta_title }}"/> @endif

@endsection

@section('main')
    @include('webapix.partials.breadcrumb', ['elements' => [
        route('home') => __('home.title'),
        route('news') => __('news.title'),
        route('news_details', $item->speakingURL) => $item->title
    ]])

    <div class="panel">
        <div class="panel-heading">
            <h4>{{ $item->title }}</h4>
            <small>{{ $item->date_from }}
                @if( !empty($item->author) )
                    |
                    @if(is_array($item->author))
                        @foreach( $item->author as $author )
                            <a href="{{ URL::route('news') }}?author={{ $author }}">{{ $author }}</a>
                        @endforeach
                    @else
                        {{ $item->author }}
                    @endif
                @endif
            </small>
        </div> <!-- /.panel-heading -->

        <div class="panel-body">
            <div class="plain-text"><strong>{!! $item->lead !!}</strong></div>

            <div class="row" style="margin-bottom: 16px;">
                <img class="img-responsive" src="{{ asset('thumbnails/940/0/' . $item->lead_picture) }}"
                     style="width: 100%" alt="{{ $item->title }}"/>
            </div>

            <div class="plain-text">{!! $item->content !!}</div>
        </div> <!-- /.panel-body -->

        <div class="panel-footer">
            <a class="btn btn-primary" href="https://facebook.com/sharer/sharer.php?u={{ url()->current() }}"
               target="_blank" aria-label="Facebook">
                Facebook
            </a>
            <a class="btn btn-primary"
               href="https://twitter.com/intent/tweet/?text={{ url()->current() }}&amp;url={{ url()->current() }}"
               target="_blank" aria-label="Twitter">
                Twitter
            </a>
            <a class="btn btn-primary" href="https://plus.google.com/share?url={{ url()->current() }}" target="_blank"
               aria-label="Google+">
                Google+
            </a>
        </div> <!-- /.panel-footer -->
    </div><!-- /.panel -->
@endsection

