@extends('webapix.emails.main')

@section('head')
@stop

@section('content')
    Kedves {{ $data->name }}!<br /><br />
    Sikeresen megkaptuk üzenetét! Hamarosan visszajelzünk!<br />
    <br />
    <strong>Adataid:</strong>
    <br />
    Név: {{ $data->name }} <br />
    E-mail cím: {{ $data->email }} <br />
    Telefonszám: {{ $data->phone }} <br />
    Üzenet: {{ $data->message }} <br />
@stop