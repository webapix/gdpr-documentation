<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>webapix.portal</title>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="#ffffff" style="background-color: #ffffff;">
    <center>
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" bgcolor="#ffffff">
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="background-color: #ffffff;">
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="600">
                                    <tr>
                                        <td>
                                            <!-- header -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="600">
                                                <tr>
                                                    <td colspan="3" height="10"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"><img src="{{ asset('images/placeholder/logo.png') }}" alt="webapix.portal" style="display: block;"/></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" height="10"></td>
                                                </tr>                                                
                                                <tr>
                                                    <td colspan="3" height="1" style="height: 1px; border-top: 1px solid #3680a4; font-size: 1px; line-height: 1px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" height="10"></td>
                                                </tr>
                                                <tr>
                                                    <td width="10"></td>
                                                    <td width="580" style="font-size: 15px; color: #333333; font-family: Arial, Helvetica, sans-serif;">

                                                        @yield('head')
                                                        
                                                        @yield('content')
                                                        
                                                        <br />

                                                    </td>
                                                    <td width="10"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" height="10"></td>
                                                </tr>
                                            </table>
                                            <!-- eof header -->
                                    
                                            <!-- footer bottom -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="background:#332e2b;">
                                                <tr>
                                                    <td colspan="3" height="20">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="37"></td>
                                                    <td width="526" style="font-size: 13px; color: #ffffff; font-family: Arial, Helvetica, sans-serif;">
                                                        webapix.portal
                                                    </td>
                                                    <td width="37"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" height="20">&nbsp;</td>
                                                </tr>
                                            </table>
                                            <!-- eof footer bottom -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </center>
</body>

</html>