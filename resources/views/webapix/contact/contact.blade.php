@extends('webapix.base')

@section('main')
    @include('webapix.partials.breadcrumb', ['elements' => [
        route('home') => 'Főoldal',
        route('contact') => 'Kapcsolat'
    ]])
    <div class="panel panel-article">
        <div class="panel-heading">
            <h3 class="article-title"> @lang('contact.title') </h3>
            <div class="article-lead">{!! embedContent('contact-success', 'Adminolhato szoveg amit be kell helyettesiteni') !!}</div>
        </div>

        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            @if (!empty($success))
                <div class="alert alert-success">
                    @lang('contact.success_msg')
                </div>
            @else
                <form class="form-horizontal" method="post" action="{{ route('contact_save') }}" data-validate="true">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="fullname" class="col-sm-3 control-label">Név</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="fullname" name="name" required
                                   data-empty-message=" @lang('contact.error.name_required')">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">E-mail</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" id="email" name="email" required
                                   data-empty-message=" @lang('contact.error.email_required')"
                                   data-error-message=" @lang('contact.error.invalid_email')">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="note" class="col-sm-3 control-label"> @lang('contact.phone') </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="phone" name="phone"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="note" class="col-sm-3 control-label"> @lang('contact.message') </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="message" name="message" required
                                      data-empty-message=" @lang('contact.error.message_required') "></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary"> @lang('contact.submit') </button>
                        </div>
                    </div>
                </form>
            @endif

        </div> <!-- /.panel-body -->
    </div><!-- /.panel panel-article -->


@endsection
