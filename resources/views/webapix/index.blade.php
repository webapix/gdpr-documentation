@extends('webapix.base')

@section('main')
    @include('webapix.partials.breadcrumb', ['elements' => [
        route('home') => __('home.title'),
    ]])


    <div class="news-list-block">
        @foreach($articles as $article)

            <div class="panel">
                <div class="panel-heading">
                    <h4>
                        <a href="{{ route('article', $article) }}">{{ $article->title }}</a>
                        <a href="{{ route('article', $article) }}" class="pull-right"><span
                                    class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                </div>
            </div>

            @foreach($article->childrens as $subarticle)
                <div class="panel sub">
                    <div class="panel-heading">
                        <h4>
                            <a href="{{ route('article', $subarticle) }}">{{ $subarticle->title }}</a>
                            <a href="{{ route('article', $subarticle) }}" class="pull-right"><span
                                        class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                        </h4>
                    </div>
                </div>
            @endforeach

        @endforeach
    </div>


    {!! $articles->render() !!}


@endsection
