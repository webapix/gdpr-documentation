@extends('webapix.base')

@section('main')
    
    <div class="panel panel-carousel">
        <div class="panel-body">
            @include('webapix.carousel.partial')
        </div> <!-- /.panel-body -->
    </div><!-- /.panel panel-carousel -->
    
@endsection
