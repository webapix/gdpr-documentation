@extends('webapix.base')

@section('main')
    
    <div class="panel panel-carousel">
        <div class="panel-body">
            @include('webapix.carousel.partial_without_viewcomposer', [ 'carousels' => [ $carousels ] ])
        </div> <!-- /.panel-body -->
    </div><!-- /.panel panel-carousel -->
    
@endsection
