@if (isset($carousels) and !empty($carousels))
    <div id="carousel" class="carousel slide" data-ride="carousel">
        @if( count($carousels) != 1 )
            <ol class="carousel-indicators">
                @foreach($carousels as $i => $carousel)
                    <li data-target="#carousel" data-slide-to="{{ $i }}" @if( $i==0 ) class="active" @endif></li>
                @endforeach
            </ol>
        @endif

        <div class="carousel-inner" role="listbox">
            @foreach($carousels as $i => $carousel)
                <div class="item @if( $i==0 ) active @endif">
                    <a href="{{ $carousel['target_url']}}">
                        <img src="{{ getThumbnail($carousel['picture'], 940) }}" alt="{{ $carousel['name'] }}">
                        <div class="carousel-caption bg-primary">
                            <h3>{{ $carousel['name'] }}</h3>
                            <p>{!! $carousel['text'] !!}</p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>

        @if( count($carousels) != 1 )
            <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only"> @lang('carousel.previous') </span>
            </a>
            <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only"> @lang('carousel.next') </span>
            </a>
        @endif
    </div>
@endif
