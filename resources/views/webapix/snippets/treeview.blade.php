@if($tree instanceOf \Illuminate\Database\Eloquent\Collection)
    @if(!$tree->isEmpty())
        <ul class="tree @if(isset($open) && $open) open @endif ">
            @foreach($tree as $node)
                <li @if($openNode != null && $node->id == $openNode->id)class="selected" @endif ><a
                            href="{{ $prefix }}{{ $node->speakingURL }}">{{ $node->getLabel() }}</a></li>
                @include('webapix.snippets.treeview', ['tree' => $node->children, 'prefix' => $prefix, 'open' => $node->isParentOf($openNode), 'openNode' => $openNode])
            @endforeach
        </ul>
    @else
        <!-- Empty Collection, no tree to render -->
    @endif
@else
    <!-- The treeview snippet requires an Eloquent Collection object! -->
@endif