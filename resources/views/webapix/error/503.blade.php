@extends('webapix.base')

@section('main')
    <div class="panel">
        <div class="panel-heading">
            <h4> @lang('errors.503_title') </h4>
            <small> @lang('errors.503_subtitle') </small>
        </div> <!-- /.panel-heading -->

        <div class="panel-body">
            <div class="plain-text"><p>  @lang('errors.home_link', ['href' => route('home')]) </p>
            </div>
        </div> <!-- /.panel-body -->
    </div><!-- /.panel -->
@endsection
