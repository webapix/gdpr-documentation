@extends('webapix.base')

@section('main')

    @if ($errors->any())
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <strong>Hiba!</strong>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="panel">
            	<div class="panel-heading">
                	<h4>@lang('wxauth.login')</h4>
                </div>
                
                <div class="panel-body">


                    <form class="form-horizontal" action="{{ URL::route('login')  }}" method="post" role="form">
                        {!! csrf_field() !!}
                        
                        <div class="form-group has-feedback">
                            <div class="col-sm-6 col-xs-12">
                                <input type="text" placeholder="E-mail" name="email" value="{{ old('email') }}"
                                       id="field-1" class="form-control">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <div class="col-sm-6 col-xs-12">
                                <input type="password" placeholder="Jelszó" name="password" id="field-3"
                                       class="form-control">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                        </div>

                        <input type="hidden" name="redirect"
                               value="@if(!isset($redirect)) {{ \Illuminate\Support\Facades\URL::previous() }} @else{{ $redirect }}@endif">

						{{--
                        <a id="password-reset-link" href="{{ route('password.request') }}">Elfelejtett jelszó</a>
                        <br />
                        <a href="{{ route('register') }}">Regisztráció</a>
                        --}}

                        <div class="form-group" style="margin-bottom:0;">
                            <div class="col-sm-6 col-xs-12">
                                <button class="btn btn-primary btn-block" type="submit">Belépés</button>
                            </div>
                        </div>
                    </form><!-- .form -->
                </div>
            </div>
        </div>
    </div>
@endsection