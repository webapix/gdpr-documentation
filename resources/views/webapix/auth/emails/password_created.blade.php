Kedves {{ $user?$user->name:'Felhasználó' }}!

Az ön fiókjához tartozó jelszava sikeresen létrejött a <a href="{{ env('APP_URL') }}">{{ env('APP_URL') }}</a> oldalon.
Belépéshez használja a {{ $user->email }} felhasználónevet
és a {{ $password }} jelszót.

Jelszavát bármikor megváltoztathatja belépés után a profil oldalon.

