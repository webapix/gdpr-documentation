Hello {{ $user?$user->name:'Felhasználó!' }}!

Azért kapod ezt a levelet, mert jelszót akartál váltani.

Kattints ide, hogy új jelszót kapj: {!!  route('password.reset', ['token' => $token]) !!}

