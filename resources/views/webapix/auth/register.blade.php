@extends('webapix.base')

@section('main')
    @include('webapix.partials.breadcrumb', ['elements' => [
        route('home') => __('home.title'),
        route('register') => __('register.title')
    ]])

    <div class="panel">
    	<div class="panel-heading">
                	<h4>@lang('wxauth.register')</h4>
                </div>
                
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-sm-3 control-label"> @lang('register.name') </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required
                               autofocus>

                        @if ($errors->has('name'))
                            <span class="alert alert-danger">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="email" class="col-sm-3 control-label"> @lang('register.email') </label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                               required>

                        @if ($errors->has('email'))
                            <span class="alert alert-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-sm-3 control-label"> @lang('register.password') </label>

                    <div class="col-sm-9">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="alert alert-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm"
                           class="col-sm-3 control-label"> @lang('register.confirm_password') </label>

                    <div class="col-sm-9">
                        <input id="password-confirm" name="password_confirmation" type="password" class="form-control"
                               required>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('recaptcha_response_field') ? ' has-error' : '' }}">
                    <div class="col-sm-9 col-md-offset-3">
                        {!! Recaptcha::render() !!}

                        @if ($errors->has('recaptcha_response_field'))
                            <span class="alert alert-danger">
                                <strong>{{ $errors->first('recaptcha_response_field') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-primary">
                            @lang('register.submit')
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
