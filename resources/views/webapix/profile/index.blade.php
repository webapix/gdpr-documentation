@extends('webapix.base')

@section('main')
    @include('webapix.partials.breadcrumb', ['elements' => [
        route('home') => __('home.title'),
        route('profile') => __('profile.title')
    ]])

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }} <br>
            @endforeach
        </div>
    @endif
    @if (!empty(session('profile-success')))
        <div class="alert alert-success">
            @lang('profile.change_success')
        </div>
    @endif

    <div class="panel">
        <div class="panel-heading">
            <h4> @lang('profile.my_info') </h4>
        </div> <!-- /.panel-heading -->

        <div class="panel-body">

            {{ Form::open(array(
                'url' => route('profile_save'),
                'method' => 'post',
                'class' => 'form-horizontal'
            )) }}

            <div class="form-group">
                {{ Form::label('name', __('profile.name'), array(
                    'class' => 'col-sm-3 control-label',
                )) }}
                <div class="col-sm-9">
                    {{ Form::text('name', $user->name, array(
                        'class' => 'form-control',
                        'required' => true
                    )) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> @lang('profile.submit') </button>
                </div>
            </div>

            {{ Form::close() }}

        </div> <!-- /.panel-body -->
    </div><!-- /.panel -->

    <div class="panel">
        <div class="panel-heading">
            <h4> @lang('profile.change_password') </h4>
        </div> <!-- /.panel-heading -->

        <div class="panel-body">

            {{ Form::open(array(
                'url' => route('password_change'),
                'method' => 'post',
                'class' => 'form-horizontal'
            )) }}

            <div class="form-group">
                {{ Form::label('password', __('profile.password'), array(
                    'class' => 'col-sm-3 control-label',
                )) }}
                <div class="col-sm-9">
                    {{ Form::password('password', array(
                        'class' => 'form-control',
                        'required' => true
                    )) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('password_confirmation', __('profile.confirm_password'), array(
                    'class' => 'col-sm-3 control-label',
                )) }}
                <div class="col-sm-9">
                    {{ Form::password('password_confirmation', array(
                        'class' => 'form-control',
                        'required' => true
                    )) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> @lang('profile.submit') </button>
                </div>
            </div>

            {{ Form::close() }}

        </div> <!-- /.panel-body -->
    </div><!-- /.panel -->
@endsection
