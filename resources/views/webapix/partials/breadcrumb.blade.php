<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        @foreach($elements as $url => $name)

            <li {{ !$loop->first ? 'itemprop="child" ' : '' }}
                itemscope itemtype="http://data-vocabulary.org/Breadcrumb"
                    {{ $loop->last ? 'class="active" ' : '' }}
            >
                @if($loop->last)
                    <span itemprop="title">{{ $name }}</span>
                @else
                    <a href="{{ $url }}" itemprop="url"><span itemprop="title">{{ $name }}</span></a>
                @endif
            </li>

        @endforeach
    </ol>
</div>
