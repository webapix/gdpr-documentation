<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset('images/favicons/manifest.json') }}">
<link rel="mask-icon" href="{{ asset('images/favicons/safari-pinned-tab.svg') }}" color="{{ $mask }}">
<link rel="shortcut icon" href="{{ asset('images/favicons/favicon.ico') }}">
<meta name="msapplication-config" content="{{ asset('images/favicons/browserconfig.xml') }}">
<meta name="theme-color" content="{{ $theme }}">
