<ul class="nav navbar-nav">
    @foreach($navigation_menu as $menu)
        <li @if(Request::path() == $menu->url)class="active"@endif>
            <a href="{{ $menu->url }}">{{ $menu->title }}</a>
        </li>
    @endforeach
</ul>
