@if( !empty($pagesList) )
    <nav style="text-align: center;">
        <ol class="pagination" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
            @foreach( $pagesList as $item )
                <li class="@if( $item['page']==$selectedPage) active @endif">
                    <a href="{{ $item['target'] }}" itemprop="url"><span itemprop="name">{{ $item['label'] }}</span></a>
                </li>
            @endforeach
        </ol>
    </nav>
@endif
