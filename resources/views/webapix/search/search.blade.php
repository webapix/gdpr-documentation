@extends('webapix.base')

@section('main')

    @if( !empty($query) )
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4> @lang('search.results_title', ['query' => $query]) </h4>
            </div>
        </div>

        @forelse($results as $type => $result)
                <header class="general-title">
                    <h1>{{ $type }}</h1>
                </header>
                @forelse($result as $item)
                    <div class="panel clearfix">
                        <div class="panel-heading">
                            <h4>
                            	<a href="{{ $item['url'] }}">{{ $item['title'] }}</a>
                                <a href="{{ $item['url'] }}" class="pull-right"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                            </h4>
                        </div> <!-- /.panel-heading -->

						{{--
                        <div class="panel-body" style="padding-bottom: 0;">
                            <div class="plain-text"><strong>{!! $item['lead'] !!}</strong></div>

                            @if (isset($result->lead_picture))
                                <div class="row">
                                    <a href="{{ $item['url'] }}">
                                        <img class="img-responsive" src="{{ getThumbnail($item->lead_picture, 940) }}"
                                             style="width: 100%" alt="{{ $result->title }}"/>
                                    </a>
                                </div>
                            @endif
                        </div> <!-- /.panel-body -->

                        <div class="panel-footer">
                            <a class="btn btn-sm btn-primary" href="{{ $item['url'] }}"
                               role="button">{{ __('search.details') }}</a>
                        </div> <!-- /.panel-footer -->
                        --}}
                    </div><!-- /.panel -->
                @empty
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4> @lang('search.no_results') </h4>
                        </div>
                    </div>
                @endforelse


            @if(empty($search_in) and $result->hasMorePages())
                <div class="clearfix" style="margin-top: 1em; margin-bottom: 1em;">
                    <div class="pull-right">
                        <a class="btn gallery-btn btn-primary btn-sm"
                           href="{{ route('search', ['query' => $query, 'type' => str_slug($type)]) }}"
                           role="button"> @lang('search.further_results') </a>
                    </div>
                </div>
            @endif

            @if (!empty($search_in))
                <div class="clearfix">
                    <div class="text-center">
                        {{ $result->appends(['type' => str_slug($type)])->links() }}
                    </div>
                </div>
            @endif
        @empty
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h4> @lang('search.no_results') </h4>
                </div>
            </div>
        @endforelse
    @else
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h4> @lang('search.no_results') </h4>
            </div>
        </div>
    @endif

@endsection
