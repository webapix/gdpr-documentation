<div class="panel panel-search">
    <h4 class="panel-heading">{{ __('search.title') }}</h4>
    <div class="panel-body">

        {{ Form::open(array('url' => route('postSearch'), 'method' => 'post')) }}

        <div class="form-group">
            <div class="input-group">
                {{ Form::label('query', __('search.label'), array(
                    'class' => 'sr-only',
                )) }}
                {{ Form::text('query', null, array(
                    'class' => 'form-control',
                    'placeholder' => __('search.placeholder')
                )) }}
                <div class="input-group-btn">
                    <button class="btn btn-default btn-search" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </div>
            </div>
        </div>

        {{ Form::close() }}

    </div> <!-- /.panel-body -->
</div> <!-- /.panel-search -->
