@extends('webapix.base', [
    'metatitle' => $article->title,
    'metadesc' => $article->getMetaDescription(),
    'metaurl' => route('article', $article->speakingURL)
])

@section('main')
    @if($article->parent)
        @include('webapix.partials.breadcrumb', ['elements' => [
            route('home') => __('home.title'),
            route('article', $article->parent->speakingURL) => $article->parent->title,
            route('article', $article->speakingURL) => $article->title
        ]])
    @else
        @include('webapix.partials.breadcrumb', ['elements' => [
            route('home') => __('home.title'),
            route('article', $article->speakingURL) => $article->title
        ]])
    @endif

    <div class="panel article-block">
        <div class="panel-heading">
            <h1 class="h2">{{ $article->title }}</h1>
            <small>{{ $article->created_at->format('Y-m-d H:i') }}</small>
        </div> <!-- /.panel-heading -->

        <div class="panel-body">
            <div class="plain-text">{!! $article->lead !!}</div>

            <div class="plain-text">{!! $article->content !!}</div>
        </div> <!-- /.panel-body -->

		{{--
        <div class="panel-footer">
        </div> <!-- /.panel-footer -->
        --}}
        
    </div><!-- /.panel -->
    <div class="news-list-block">
        @if($article->childrens)
            @foreach($article->childrens as $children)
                <div class="panel">
                    <div class="panel-heading">
                        <h4>
                            <a href="{{ route('article', $children) }}">{{ $children->title }}</a>
                            <a href="{{ route('article', $children) }}" class="pull-right"><span
                                        class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                        </h4>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    
    @push('scripts')
    <script type="text/javascript">
		jQuery('.plain-text table').addClass('table table-bordered table-hover');
		jQuery('.plain-text .table').wrap('<div class="table-responsive"></div>');
	</script>
	@endpush
    
@endsection
